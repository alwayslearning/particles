package com.particles.particles.data;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glVertexAttribPointer;
import static com.particles.particles.Constants.BYTES_PER_FLOAT;

/**
 * Created by Cuckoo322 on 4/11/2017.
 */

public class VertexArray {

    private final FloatBuffer floatBuffer;

    private final float[] vData;

    public VertexArray(float[] vertexData) {
        floatBuffer = ByteBuffer
                .allocateDirect(vertexData.length * BYTES_PER_FLOAT)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer()
                .put(vertexData);
        vData = vertexData;
    }

    public void setVertexAttribPointer(int dataOffset, int attributeLocation, int componentCount, int stride) {
        floatBuffer.position(dataOffset);

        //获得属性在顶点数组中以及OpenGL程序的位置
        // 告诉OpenGL到floatBuffer中找到位置数据属性，并赋给attributeLocation代表的变量
        //重点！！！让OpenGL到本地内存中获取顶点的值
        //参数1：属性位置
        //参数2：每个属性数据的大小组成
        //参数3：数据类型
        //参数4：只有使用整型数据时需要对数据进行标准化，使用浮点数就用false就可以
        //参数5：跨距，读取数据时使用的跳跃长度
        //参数6：在本地内存中存储顶点数据的变量
        glVertexAttribPointer(attributeLocation, componentCount, GL_FLOAT, false, stride, floatBuffer);
        glEnableVertexAttribArray(attributeLocation);
        floatBuffer.position(0);
    }

    /**
     * 将数据更新到本地缓冲区
     */
    public void updateBuffer(float[] vertexData, int start, int count) {
        floatBuffer.position(start);
        floatBuffer.put(vertexData, start, count);
        floatBuffer.position(0);
    }
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (float f : vData) {
            sb.append(f + ", ");
        }
        sb.append("}\n");
        return sb.toString();
    }

}
