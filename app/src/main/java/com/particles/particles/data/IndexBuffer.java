package com.particles.particles.data;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import static android.opengl.GLES20.GL_ARRAY_BUFFER;
import static android.opengl.GLES20.GL_ELEMENT_ARRAY_BUFFER;
import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_SHORT;
import static android.opengl.GLES20.GL_STATIC_DRAW;
import static android.opengl.GLES20.glBindBuffer;
import static android.opengl.GLES20.glBufferData;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGenBuffers;
import static android.opengl.GLES20.glVertexAttribPointer;
import static com.particles.particles.Constants.BYTES_PER_FLOAT;
import static com.particles.particles.Constants.BYTES_PER_SHORT;

/**
 * Created by Cuckoo322 on 4/19/2017.
 */

public class IndexBuffer {

    private final int bufferId;

    public IndexBuffer(short[] vertexData) {
        //分配一个缓冲区
        final int[] buffers = new int[1];
        glGenBuffers(buffers.length, buffers, 0);
        if (buffers[0] == 0) {
            throw new RuntimeException("不能创建新的顶点缓冲对象");
        }
        bufferId = buffers[0];

        //绑定缓冲区
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferId);

        //将数据写到本地内存中
        ShortBuffer vertexArray = ByteBuffer
                .allocateDirect(vertexData.length * BYTES_PER_SHORT)
                .order(ByteOrder.nativeOrder())
                .asShortBuffer()
                .put(vertexData);

        vertexArray.position(0);

        //将数据从内存转移到GPU缓冲区
        glBufferData(
                GL_ELEMENT_ARRAY_BUFFER,
                vertexArray.capacity() * BYTES_PER_SHORT,
                vertexArray,
                GL_STATIC_DRAW
        );

        //解绑缓冲区
        //若不解绑，容易导致其他调用绑定缓冲区的函数不能正常工作
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }


    public void setVertexAttribPointer(int dataOffset, int attributeLocation, int componentCount, int stride) {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferId);

        //获得属性在顶点数组中以及OpenGL程序的位置
        // 告诉OpenGL到floatBuffer中找到位置数据属性，并赋给attributeLocation代表的变量
        //重点！！！让OpenGL到本地内存中获取顶点的值
        //参数1：属性位置
        //参数2：每个属性数据的大小组成
        //参数3：数据类型
        //参数4：只有使用整型数据时需要对数据进行标准化，使用浮点数就用false就可以
        //参数5：跨距，读取数据时使用的跳跃长度
        //参数6：在本地内存中存储顶点数据的变量
        glVertexAttribPointer(attributeLocation, componentCount, GL_SHORT, false, stride, dataOffset);
        glEnableVertexAttribArray(attributeLocation);

        //解绑缓冲区
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    public int getBufferId() {
        return bufferId;
    }
}
