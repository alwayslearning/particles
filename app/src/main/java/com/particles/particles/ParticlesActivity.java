package com.particles.particles;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

public class ParticlesActivity extends AppCompatActivity {

    //添加一个GLSurfaceView实例，舒适化OpenGL
    private GLSurfaceView glSurfaceView = null;
    private boolean rendererSet = false;

    //渲染器
    ParticlesRenderer particlesRenderer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_particles);

        glSurfaceView = new GLSurfaceView(getApplicationContext());

        //检查系统是否支持OpenGL ES 2.0
        final ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        final ConfigurationInfo configurationInfo = activityManager.getDeviceConfigurationInfo();
        final boolean supportEs2 = configurationInfo.reqGlEsVersion >= 0x20000
                || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1
                && (Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")));

        //如果支持ES2.0，则配置渲染表面
        if (supportEs2) {
            //配置surface视图
            glSurfaceView.setEGLContextClientVersion(2);

            //保存渲染器
            particlesRenderer = new ParticlesRenderer(getApplicationContext());

            //新建自定义的Renderer实例
            glSurfaceView.setRenderer(particlesRenderer);
            //记住渲染器已经设置过了
            rendererSet = true;
        } else {
            Toast.makeText(this, "此设备不支持OpenGL ES 2.0", Toast.LENGTH_SHORT).show();
            return;
        }

        //设置触控监听用于旋转相机
        glSurfaceView.setOnTouchListener(new View.OnTouchListener() {
            float previousX, previousY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event != null) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        previousX = event.getX();
                        previousY = event.getY();
                    } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
                        final float deltaX = event.getX() - previousX;
                        final float deltaY = event.getY() - previousY;
                        previousX = event.getX();
                        previousY = event.getY();

                        glSurfaceView.queueEvent(new Runnable() {
                            @Override
                            public void run() {
                                particlesRenderer.handleTouchDrag(deltaX, deltaY);
                            }
                        });
                    }
                    return true;
                } else {
                    return false;
                }
            }
        });

        //把GLSurfaceView加入到activity中，显示到屏幕上
        setContentView(glSurfaceView);
    }

    @Override
    protected void onPause() {
        super.onPause();

        //在应用暂停时需要暂停GLSurfaceView，否则应用会崩溃，这里需要释放OpenGL上文
        if (rendererSet) {
            glSurfaceView.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //应用继续被打开时需要继续使用OpenGL下文
        if (rendererSet) {
            glSurfaceView.onResume();
        }
    }
}
