package com.particles.particles.programs;

import android.content.Context;

import com.particles.particles.util.ShaderHelper;
import com.particles.particles.util.TextResourceReader;

import static android.opengl.GLES20.glUseProgram;

/**
 * Created by Cuckoo322 on 4/11/2017.
 */

public class ShaderProgram {

    //uniform的常量
    protected static final String U_MATRIX = "u_Matrix";
    protected static final String U_TEXTURE_UNIT = "u_TextureUnit";
    protected static final String U_COLOR = "u_Color";
    protected static final String U_TIME = "u_Time";

    //attribute的常量
    protected static final String A_POSITION = "a_Position";
    protected static final String A_COLOR = "a_Color";
    protected static final String A_TEXTURE_COORDINATES = "a_TextureCoordinates";
    protected static final String A_DIRECTION_VECTOR = "a_DirectionVector";
    protected static final String A_PARTICLE_START_TIME = "a_ParticleStartTime";

    //着色器program
    protected final int program;

    public ShaderProgram(Context context, int vertexResourceId, int fragmentResourceId) {
        program = ShaderHelper.buildProgram(
                TextResourceReader.readTextFileFromResource(context, vertexResourceId),
                TextResourceReader.readTextFileFromResource(context, fragmentResourceId)
        );
    }

    public void useProgram() {
        glUseProgram(program);
    }
}
