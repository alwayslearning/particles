package com.particles.particles.programs;

import android.content.Context;

import com.particles.particles.R;

import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glUniformMatrix4fv;

/**
 * Created by Cuckoo322 on 4/19/2017.
 */

public class HeightmapShaderProgram extends ShaderProgram {

    //uniform位置
    private final int uMatrixLocation;

    //attribute位置
    private final int aPositionLocation;


    public HeightmapShaderProgram(Context context) {
        super(context, R.raw.heightmap_vertex_shader, R.raw.heightmap_fragment_shader);

        uMatrixLocation = glGetUniformLocation(program, U_MATRIX);
        aPositionLocation = glGetAttribLocation(program, A_POSITION);
    }

    public void setUniforms(float[] matrix) {
        glUniformMatrix4fv(uMatrixLocation, 1, false, matrix, 0);
    }

    public int getaPositionAttributeLocation() {
        return aPositionLocation;
    }

}
