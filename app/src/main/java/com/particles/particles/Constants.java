package com.particles.particles;

/**
 * Created by Cuckoo322 on 4/11/2017.
 */

public class Constants {

    public static final int BYTES_PER_FLOAT = 4;
    public static final int BYTES_PER_SHORT = 2;

}
