package com.particles.particles.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import static android.opengl.GLES20.GL_LINEAR;
import static android.opengl.GLES20.GL_LINEAR_MIPMAP_LINEAR;
import static android.opengl.GLES20.GL_TEXTURE;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.GL_TEXTURE_CUBE_MAP;
import static android.opengl.GLES20.GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
import static android.opengl.GLES20.GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
import static android.opengl.GLES20.GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;
import static android.opengl.GLES20.GL_TEXTURE_CUBE_MAP_POSITIVE_X;
import static android.opengl.GLES20.GL_TEXTURE_CUBE_MAP_POSITIVE_Y;
import static android.opengl.GLES20.GL_TEXTURE_CUBE_MAP_POSITIVE_Z;
import static android.opengl.GLES20.GL_TEXTURE_MAG_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_MIN_FILTER;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glGenTextures;
import static android.opengl.GLES20.glGenerateMipmap;
import static android.opengl.GLES20.glTexParameteri;
import static android.opengl.GLUtils.texImage2D;

/**
 * Created by Cuckoo322 on 4/11/2017.
 */

public class TextureHelper {

    private static final String TAG = "TextureHelper";

    public static int loadTexture(Context context, int resourceId) {
        //生成纹理ID
        final int[] textureObjectIds = new int[1];
        glGenTextures(1, textureObjectIds, 0);
        if (textureObjectIds[0] == 0) {
            if (LoggerConfig.ON) {
                Log.w(TAG, "不能创建OpenGL纹理对象");
            }
            return  0;
        }

        //利用安卓自带的位图解析器解压缩png，OpenGL没有自带图片读取的功能，需要Android先将png解压缩为位图
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId, options);
        if (bitmap == null) {
            if (LoggerConfig.ON) {
                Log.w(TAG, "资源ID："+resourceId+" 不能被解析成位图");
            }
            glDeleteTextures(1, textureObjectIds, 0);
            return 0;
        }

        //让OpenGL绑定纹理
        glBindTexture(GL_TEXTURE_2D, textureObjectIds[0]);

        //缩小使用三线性过滤过滤，放大使用双线性过滤
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        //加载纹理到OpenGL
        texImage2D(GL_TEXTURE_2D, 0, bitmap, 0);

        //OpenGL已经加载了纹理，显式调用Delvik的位图回收释放数据
        bitmap.recycle();

        //让OpenGL生成所有必要级别的MIP贴图
        glGenerateMipmap(GL_TEXTURE_2D);

        //解绑此纹理，防止其它方法调用意外改变这个纹理
        glBindTexture(GL_TEXTURE_2D, 0);

        return textureObjectIds[0];
    }

    public static int loadCubeMap(Context context, int[] cubeResources) {
        //生成纹理ID
        final int[] textureObjectIds = new int[1];
        glGenTextures(1, textureObjectIds, 0);

        if (textureObjectIds[0] == 0) {
            if (LoggerConfig.ON) {
                Log.w(TAG, "不能创建OpenGL纹理对象");
            }
            return  0;
        }

        //利用安卓自带的位图解析器解压缩png，OpenGL没有自带图片读取的功能，需要Android先将png解压缩为位图
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;

        //天空盒的6张位图
        final Bitmap[] cubeBitmaps = new Bitmap[6];
        for (int i = 0; i < 6; i++) {
            cubeBitmaps[i] = BitmapFactory.decodeResource(context.getResources(), cubeResources[i], options);
            if (cubeBitmaps[i] == null) {
                if (LoggerConfig.ON) {
                    Log.w(TAG, "资源ID："+cubeResources[i]+" 不能被解析成位图");
                }
                glDeleteTextures(1, textureObjectIds, 0);
                return 0;
            }
        }

        //让OpenGL绑定纹理到天空盒
        glBindTexture(GL_TEXTURE_CUBE_MAP, textureObjectIds[0]);

        //因为天空盒的贴图要求不高，所以缩小和放大都使用双线性过滤，可以节省纹理内存
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        //把每张图像与其对应的立方体贴图的面关联起来，顺序是左右、下上、前后
        //立方体内使用左手坐标，立方体外使用右手坐标
        texImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, cubeBitmaps[0], 0);
        texImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, cubeBitmaps[1], 0);

        texImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, cubeBitmaps[2], 0);
        texImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, cubeBitmaps[3], 0);

        texImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, cubeBitmaps[4], 0);
        texImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, cubeBitmaps[5], 0);


        //解绑此纹理，防止其它方法调用意外改变这个纹理
        glBindTexture(GL_TEXTURE_2D, 0);

        //释放纹理位图
        for (Bitmap bitmap : cubeBitmaps) {
            bitmap.recycle();
        }

        return textureObjectIds[0];
    }

}
