package com.particles.particles.util;

/**
 * Created by Cuckoo322 on 4/10/2017.
 */

public class MatrixHelper {

    /**
     * 透视投影矩阵
     * @param m 结果矩阵，目标数组
     * @param yFovInDegrees 视锥体的角度
     * @param aspect 屏幕宽高比
     * @param n 到近处平面的距离，必须是正值
     * @param f 到远处平面的距离，必须是正值且大于到近处屏幕的距离
     */
    public static void perspectiveM(float[] m, float yFovInDegrees, float aspect, float n, float f) {
        //计算焦距
        final float angleInRadians = (float) (yFovInDegrees * Math.PI / 180.0);
        final float a = (float) (1.0 / Math.tan(angleInRadians / 2.0));

        //投影矩阵
        m[0] = a / aspect;
        m[1] = 0f;
        m[2] = 0f;
        m[3] = 0f;

        m[4] = 0f;
        m[5] = a;
        m[6] = 0f;
        m[7] = 0f;

        m[8] = 0f;
        m[9] = 0f;
        m[10] = -((f + n) / (f - n));
        m[11] = -1f;

        m[12] = 0f;
        m[13] = 0f;
        m[14] = -((2* f * n) / (f - n));
        m[15] = 0f;
    }

}
