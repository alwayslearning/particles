package com.particles.particles.objects;

import com.particles.particles.util.Geometry;

import java.util.Random;

import static android.opengl.Matrix.multiplyMV;
import static android.opengl.Matrix.setRotateEulerM;

/**
 * Created by Cuckoo322 on 4/14/2017.
 */

public class ParticleShooter {

    private final Geometry.Point position;
//    private final Geometry.Vector direction;
    private final int color;

    //发射器的发射角度变化和速度变化
    private final float angleVariance;
    private final float speedVariance;

    private final Random random = new Random();

    //
    private float[] rotationMatrix = new float[16];
    private float[] directionVector = new float[4];
    private float[] resultVector = new float[4];

    public ParticleShooter(Geometry.Point position, Geometry.Vector direction, int color, float angleVarianceInDegrees, float speedVariance) {
        this.position = position;
//        this.direction = direction;
        this.color = color;

        this.angleVariance = angleVarianceInDegrees;
        this.speedVariance = speedVariance;

        directionVector[0] = direction.x;
        directionVector[1] = direction.y;
        directionVector[2] = direction.z;
    }

    /**
     * 增加粒子点
     * @param particleSystem
     * @param currentTime
     * @param count
     */
    public void addParticles(ParticleSystem particleSystem, float currentTime, int count) {
        for (int i = 0; i < count; i++) {
            //利用随机数产生有偏转角
            setRotateEulerM(
                    rotationMatrix,
                    0,
                    (random.nextFloat() - 0.5f) * angleVariance,
                    (random.nextFloat() - 0.5f) * angleVariance,
                    (random.nextFloat() - 0.5f) * angleVariance
            );

            //利用偏转角产生不同发射方向的粒子
            multiplyMV(resultVector, 0, rotationMatrix, 0, directionVector, 0);

            //利用随机数产生速度差异
            float speedAdjustment = 1f + random.nextFloat() * speedVariance;

            Geometry.Vector thisDirection = new Geometry.Vector(
                    resultVector[0] * speedAdjustment,
                    resultVector[1] * speedAdjustment,
                    resultVector[2] * speedAdjustment
            );

            particleSystem.addParticle(position, color, thisDirection, currentTime);
        }
    }

    /**
     * 增加一个由5个粒子组成的拖尾
     * @param particleSystem
     * @param currentTime
     * @param count
     */
    public void addTrack(ParticleSystem particleSystem, float currentTime, int count) {
        for (int i = 0; i < count; i++) {
            //利用随机数产生有偏转角
            setRotateEulerM(
                    rotationMatrix,
                    0,
                    (random.nextFloat() - 0.5f) * angleVariance,
                    (random.nextFloat() - 0.5f) * angleVariance,
                    (random.nextFloat() - 0.5f) * angleVariance
            );

            //利用偏转角产生不同发射方向的粒子
            multiplyMV(resultVector, 0, rotationMatrix, 0, directionVector, 0);

            //利用随机数产生速度差异
            float speedAdjustment = 1f + random.nextFloat() * speedVariance;

            Geometry.Vector thisDirection = new Geometry.Vector(
                    resultVector[0] * speedAdjustment,
                    resultVector[1] * speedAdjustment,
                    resultVector[2] * speedAdjustment
            );

            particleSystem.addParticle(position, color, thisDirection, currentTime - 0.08f);
            particleSystem.addParticle(position, color, thisDirection, currentTime - 0.06f);
            particleSystem.addParticle(position, color, thisDirection, currentTime - 0.04f);
            particleSystem.addParticle(position, color, thisDirection, currentTime - 0.02f);
            particleSystem.addParticle(position, color, thisDirection, currentTime);
        }
    }
}
