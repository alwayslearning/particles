package com.particles.particles.objects;

import com.particles.particles.data.VertexArray;
import com.particles.particles.programs.SkyboxShaderProgram;

import java.nio.ByteBuffer;

import static android.opengl.GLES20.GL_TRIANGLES;
import static android.opengl.GLES20.GL_UNSIGNED_BYTE;
import static android.opengl.GLES20.glDrawElements;

/**
 * Created by Cuckoo322 on 4/18/2017.
 */

public class Skybox {

    private static final int POSITION_COMPONENT_COUNT = 3;
    //真顶点数组
    private final VertexArray vertexArray;
    //顶点索引
    private final ByteBuffer indexArray;

    public Skybox() {
        vertexArray = new VertexArray(new float[] {
                -1f,  1f,  1f, //左、上、近
                 1f,  1f,  1f, //右、上、近
                -1f, -1f,  1f, //左、下、近
                 1f, -1f,  1f, //右、下、近
                -1f,  1f, -1f, //左、上、远
                 1f,  1f, -1f, //右、上、远
                -1f, -1f, -1f, //左、下、远
                 1f, -1f, -1f //右、下、远
        });

        indexArray = ByteBuffer.allocate(6 * 6).put(new byte[] {
                //前
                1, 3, 0,
                0, 3, 2,
                //后
                4, 6, 5,
                5, 6, 7,
                //左
                0, 2, 4,
                4, 2, 6,
                //右
                5, 7, 1,
                1, 7, 3,
                //上
                5, 1, 4,
                4, 1, 0,
                //下
                6, 2, 7,
                7, 2, 3
        });
        indexArray.position(0);
    }

    public void bindData(SkyboxShaderProgram skyboxShaderProgram) {
        vertexArray.setVertexAttribPointer(
                0,
                skyboxShaderProgram.getaPositionAttributeLocation(),
                POSITION_COMPONENT_COUNT,
                0
        );
    }

    public void draw() {
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, indexArray);
    }
}
